
# Memoizaition function memoize

def memoize(func):
    memo = {}
    def helper(x):
        if x not in memo:            
            memo[x] = func(x)
        return memo[x]
    return helper 