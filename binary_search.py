
#Iterative Binary Search

def binarySearch(array, x):
	
	l = 0				#first index
	r = len(array) - 1 	#last index	

	if (array != sorted(array)):
		
		flag = 1
		arr = sorted(array, reverse = False)	

	else:
		
		flag = 0
		arr = array.copy()

	while l <= r:

		mid = l + (r - l) // 2

		# Check if x is present at mid
		if arr[mid] == x: return mid if flag == 0 else array.index(mid)

		# If x is greater, ignore left half
		elif arr[mid] < x: l = mid + 1

		# If x is smaller, ignore right half
		else: r = mid - 1

	# If we reach here, then the element was not present
	return -1


#collection = [1,3,2,0]

# creating an empty list 
collection = [] 
  
# number of elements as input 
n = int(input("Enter number of elements : ")) 
  
# iterating till the range 
for i in range(0, n): 
    element = int(input())  
    collection.append(element) # adding the element 
 
# display the list     
print(collection) 


# prompt for the item to search
search = int(input("Enter item to search: "))


# print search results
print(binarySearch(list(collection), search))