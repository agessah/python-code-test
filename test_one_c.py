
# Memoize the Prime function 


#Memoization function
def memoize(func):
    memo = {}
    def helper(x):
        if x not in memo:            
            memo[x] = func(x)
        return memo[x]
    return helper


#Prime Numbers
@memoize
def prime(n):
    if (n==1):
        return False
    elif (n==2):
        return True;
    else:
        for x in range(2,n):
            if(n % x==0):
                return False
        return True  

#prime = memoize(prime) This also allowed in python other using the @ decorator

print(prime(int(input("Please enter an integer: "))))